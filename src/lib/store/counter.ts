import { writable } from 'svelte/store';

export const todoCounter = writable(0);
