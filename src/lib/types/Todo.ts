export type Todo = {
	name: string;
	checked: boolean;
	id: string;
};
