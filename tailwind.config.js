/** @type {import('tailwindcss').Config} */
export default {
	includeLanguages: {
		svelte: 'html'
	},
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {}
	},
	plugins: []
};
